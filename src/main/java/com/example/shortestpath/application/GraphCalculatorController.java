package com.example.shortestpath.application;

import com.example.shortestpath.GraphCalculatorService;
import com.example.shortestpath.dto.ShortestPathsRequest;
import com.example.shortestpath.dto.ShortestPathsResponse;
import jakarta.validation.Valid;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "/calculator", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
@Validated
public class GraphCalculatorController {

    private final GraphCalculatorService graphCalculatorService;

    public GraphCalculatorController(GraphCalculatorService graphCalculatorService) {
        this.graphCalculatorService = graphCalculatorService;
    }

    @PostMapping(value = "/calculateDistances")
    ShortestPathsResponse calculateDistances(@RequestBody @Valid ShortestPathsRequest request) {
        return graphCalculatorService.calculateDistances(request);
    }

}
