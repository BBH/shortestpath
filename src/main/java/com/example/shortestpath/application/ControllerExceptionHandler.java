package com.example.shortestpath.application;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.time.Instant;
import java.util.stream.Collectors;

@RestControllerAdvice
@Slf4j
public class ControllerExceptionHandler {

    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessage handleIntegrityValidationException(DataIntegrityViolationException exception, WebRequest webRequest) {
        log.warn("URL: [{}], Bad Request error: [{}]", webRequest.getDescription(true), exception.getMessage());
        return new ErrorMessage(HttpStatus.BAD_REQUEST.value(), Instant.now().getEpochSecond(), exception.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessage handleValidationException(MethodArgumentNotValidException exception, WebRequest webRequest) {
        log.warn("URL: [{}], Bad Request error: [{}]", webRequest.getDescription(true), exception.getMessage());
        return new ErrorMessage(HttpStatus.BAD_REQUEST.value(), Instant.now().getEpochSecond(), createMessageFromException(exception));
    }

    private String createMessageFromException(MethodArgumentNotValidException exception) {
        String errorFields = exception.getBindingResult().getAllErrors()
                .stream()
                .map(error -> ((FieldError) error).getField())
                .collect(Collectors.joining(", "));
        return "There are null fields: " + errorFields;
    }
}


