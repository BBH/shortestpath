package com.example.shortestpath.application;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ErrorMessage {

    private int statusCode;
    private Long timestamp;
    private String message;
}

