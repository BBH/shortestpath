package com.example.shortestpath;

import com.example.shortestpath.dto.EdgeToNode;

import java.util.*;

class ShortestPathsCalculator {

    Map<Integer, Integer> shortestBluePathLengths;
    Map<Integer, Integer> shortestRedPathLengths;
    LinkedList<Integer> reachableNodes;

    Map<Integer, Integer> calculateDistances(Map<Integer, List<EdgeToNode>> adjacencyLists, int startNode) {
        int rankOfGraph = adjacencyLists.size();
        setBaseValues(startNode, rankOfGraph, adjacencyLists.keySet());

        while (!reachableNodes.isEmpty()) {
            int reachableNode = reachableNodes.remove();
            for (EdgeToNode edgeToNode : adjacencyLists.get(reachableNode)) {
                int childNode = edgeToNode.getNode();
                boolean isChildNodeRed = !edgeToNode.isBlue;

                if (noPathExistsTo(childNode)) {
                    processChildNode(reachableNode, childNode, isChildNodeRed);
                } else {
                    setShortestDistancesForBothPaths(childNode, reachableNode, isChildNodeRed);
                }
            }
        }
        return getShortestDistances(adjacencyLists, rankOfGraph);
    }

    private void setBaseValues(int startNode, int rankOfGraph, Set<Integer> nodes) {
        shortestBluePathLengths = new HashMap<>(rankOfGraph, 1);
        shortestRedPathLengths = new HashMap<>(rankOfGraph, 1);
        reachableNodes = new LinkedList<>();
        for (int node : nodes) {
            shortestBluePathLengths.put(node, -1);
            shortestRedPathLengths.put(node, -1);
        }
        shortestBluePathLengths.put(startNode, 0);
        reachableNodes.add(startNode);
    }

    private boolean noPathExistsTo(int childNode) {
        return shortestBluePathLengths.get(childNode) < 0 && shortestRedPathLengths.get(childNode) < 0;
    }

    private void processChildNode(int reachableNode, int childNode, boolean isChildNodeRed) {
        if (onlyBluePathExistsTo(reachableNode)) {
            processChildNodeForBluePath(reachableNode, childNode, isChildNodeRed);
        } else if (onlyRedPathExistsTo(reachableNode)) {
            processChildNodeForRedPath(reachableNode, childNode, isChildNodeRed);
        } else {
            processChildNodeForBothPaths(reachableNode, childNode, isChildNodeRed);
        }
    }

    private void processChildNodeForBluePath(int reachableNode, int childNode, boolean isChildNodeRed) {
        if (isChildNodeRed) {
            shortestRedPathLengths.put(childNode, shortestBluePathLengths.get(reachableNode) + 1);
        } else {
            shortestBluePathLengths.put(childNode, shortestBluePathLengths.get(reachableNode) + 1);
        }
        reachableNodes.add(childNode);
    }

    private void processChildNodeForRedPath(int reachableNode, int childNode, boolean isChildNodeRed) {
        if (!isChildNodeRed) {
            shortestRedPathLengths.put(childNode, shortestRedPathLengths.get(reachableNode) + 1);
            reachableNodes.add(childNode);
        }
    }

    private void processChildNodeForBothPaths(int reachableNode, int childNode, boolean isChildNodeRed) {
        if (isChildNodeRed) {
            shortestRedPathLengths.put(childNode, shortestBluePathLengths.get(reachableNode) + 1);
        } else if (shortestBluePathLengths.get(reachableNode) > shortestRedPathLengths.get(reachableNode)) {
            shortestRedPathLengths.put(childNode, shortestBluePathLengths.get(reachableNode) + 1);
        } else {
            shortestBluePathLengths.put(childNode, shortestBluePathLengths.get(reachableNode) + 1);
        }
    }

    private void setShortestDistancesForBothPaths(int childNode, int reachableNode, boolean isChildNodeRed) {
        if (bluePathCanBeFoundWithReadPathAlreadyExisting(childNode, reachableNode, isChildNodeRed)) {
            shortestBluePathLengths.put(childNode, shortestBluePathLengths.get(reachableNode) + 1);
            forgetRedPathIfBothHaveTheSameLength(childNode);
        }
    }

    private boolean onlyBluePathExistsTo(int node) {
        return shortestBluePathLengths.get(node) >= 0 && shortestRedPathLengths.get(node) < 0;
    }

    private boolean onlyRedPathExistsTo(int node) {
        return shortestBluePathLengths.get(node) < 0 && shortestRedPathLengths.get(node) > 0;
    }

    private boolean bluePathCanBeFoundWithReadPathAlreadyExisting(int childNode, int reachableNode, boolean isChildNodeRed) {
        return shortestRedPathLengths.get(childNode) > 0 && shortestBluePathLengths.get(reachableNode) >= 0 && !isChildNodeRed;
    }

    private void forgetRedPathIfBothHaveTheSameLength(int childNode) {
        if (shortestRedPathLengths.get(childNode).equals(shortestBluePathLengths.get(childNode))) {
            shortestRedPathLengths.put(childNode, -1);
        }
    }

    private Map<Integer, Integer> getShortestDistances(Map<Integer, List<EdgeToNode>> adjacencyLists, int rankOfGraph) {
        Map<Integer, Integer> shortestDistancesFromStartNode = new HashMap<>(rankOfGraph, 1);
        for (int node : adjacencyLists.keySet()) {
            int value =
                    onlyBluePathExistsTo(node)
                    ? shortestBluePathLengths.get(node)
                    : shortestRedPathLengths.get(node);
            shortestDistancesFromStartNode.put(node, value);
        }
        return shortestDistancesFromStartNode;
    }
}

