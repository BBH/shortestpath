package com.example.shortestpath.dto;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class EdgeToNode {

    @NotNull
    public Integer node;
    @NotNull
    public Boolean isBlue;
}
