package com.example.shortestpath.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ShortestPathsResponse {

    public List<Distance> distances;

    public ShortestPathsResponse(List<Distance> distances) {
        this.distances = distances;
    }

    public static ShortestPathsResponse of(Map<Integer, Integer> distances) {
        List<Distance> distanceList = new ArrayList<>();
        distances.forEach((node, distance) -> distanceList.add(new Distance(node, distance)));
        return new ShortestPathsResponse(distanceList);
    }

    public record Distance(Integer node, Integer distance) {
    }
}
