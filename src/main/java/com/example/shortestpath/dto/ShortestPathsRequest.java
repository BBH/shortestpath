package com.example.shortestpath.dto;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

import java.util.List;
import java.util.Map;

public record ShortestPathsRequest(
        @NotNull Integer startNode,
        @NotNull @Valid Map<@NotNull Integer, List<@Valid EdgeToNode>> adjacencyLists) {
}
