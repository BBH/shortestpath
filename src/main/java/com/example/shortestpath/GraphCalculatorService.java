package com.example.shortestpath;

import com.example.shortestpath.application.DataIntegrityViolationException;
import com.example.shortestpath.dto.EdgeToNode;
import com.example.shortestpath.dto.ShortestPathsRequest;
import com.example.shortestpath.dto.ShortestPathsResponse;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class GraphCalculatorService {

    final ShortestPathsCalculator calculator = new ShortestPathsCalculator();

    public ShortestPathsResponse calculateDistances(ShortestPathsRequest request) {
        if (!isValid(request)) throw new DataIntegrityViolationException("Data integrity violated");
        Map<Integer, Integer> response = calculator.calculateDistances(request.adjacencyLists(), request.startNode());
        return ShortestPathsResponse.of(response);
    }

    private boolean isValid(ShortestPathsRequest request) {
        var allNodes = request.adjacencyLists().keySet();
        var startNode = request.startNode();
        return allNodes.contains(startNode) && areValidaAdjacencyListNodes(request, allNodes);
    }

    private boolean areValidaAdjacencyListNodes(ShortestPathsRequest request, Set<Integer> allNodes) {
        var endsOfEdges = request.adjacencyLists().values().stream()
                .flatMap(Collection::stream)
                .map(EdgeToNode::getNode)
                .collect(Collectors.toSet());
        return allNodes.containsAll(endsOfEdges);
    }
}
