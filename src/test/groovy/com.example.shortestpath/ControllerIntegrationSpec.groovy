package com.example.shortestpath

import com.example.shortestpath.dto.EdgeToNode
import com.example.shortestpath.dto.ShortestPathsRequest
import com.fasterxml.jackson.databind.ObjectMapper
import org.hamcrest.Matchers
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import spock.lang.Specification

import static com.example.shortestpath.AlgorithmSpec.anyWithAllEdgesInBlue
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@SpringBootTest
@AutoConfigureMockMvc
class ControllerIntegrationSpec extends Specification {

    @Autowired
    MockMvc mockMvc

    @Autowired
    ObjectMapper objectMapper

    def "Valid request is process correctly"() {
        given:
        def startNode = 0
        def adjacencyLists = anyWithAllEdgesInBlue()
        def request = new ShortestPathsRequest(startNode, adjacencyLists)

        when:
        def result = postRequest(request)

        then:
        result.andExpect(status().isOk())
                .andExpect(jsonPath('$.distances').isArray())
                .andExpect(jsonPath('$.distances.length()').value(5))
                .andExpect(jsonPath('$.distances[?(@.node == 0 && @.distance == 0)]').exists())
    }

    def " Request results in 400 Bad Request status when there are null values in the payload"() {
        given:
        def startNode = startNodeValue
        def adjacencyLists = adjacencyListsValues
        def request = new ShortestPathsRequest(startNode, adjacencyLists)

        when:
        def result = postRequest(request)

        then:
        result.andExpect(status().isBadRequest())
                .andExpect(jsonPath('$.message').value(Matchers.startsWith("There are null fields: ")))

        where:
        startNodeValue | adjacencyListsValues
        1              | null
        null           | [0: [new EdgeToNode(1, true)], 1: [new EdgeToNode(0, true)]]
        1              | [0: [new EdgeToNode(1, true)], 1: [new EdgeToNode(null, true)]]
        1              | [0: [new EdgeToNode(1, null)], 1: [new EdgeToNode(0, true)]]
    }

    def "Request results in 400 Bad Request status when #error"() {
        given:
        def request = new ShortestPathsRequest(startNode, adjacencyLists)

        when:
        def result = postRequest(request)

        then:
        result.andExpect(status().isBadRequest())
                .andExpect(jsonPath('$.message').value("Data integrity violated"))

        where:
        error                                 || startNode | adjacencyLists
        "startNode is not in the graph"       || 2         | [0: [new EdgeToNode(1, true)], 1: [new EdgeToNode(0, true)]]
        "some edges are not properly defined" || 1         | [0: [new EdgeToNode(1, true)], 1: [new EdgeToNode(2, true)]]
    }


    private ResultActions postRequest(request) {
        mockMvc.perform(MockMvcRequestBuilders.post("/calculator/calculateDistances")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
    }
}
