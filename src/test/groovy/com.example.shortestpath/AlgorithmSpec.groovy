package com.example.shortestpath

import com.example.shortestpath.dto.EdgeToNode
import spock.lang.Specification

class AlgorithmSpec extends Specification {

    def "calculation of distance from a given node to any other node is correct for valid graph"() {
        given:
        def calculator = new ShortestPathsCalculator()

        when:
        def distances = calculator.calculateDistances(adjacencyLists, startNode)

        then:
        distances == expectedDistances

        where:
        adjacencyLists            | startNode || expectedDistances
        anyWithAllEdgesInBlue()   | 0         || anyWithAllEdgesInBlueDistances()
        oneUnreachableNode()      | 0         || oneUnreachableNodeDistances()
        oneReachableAfterChange() | 0         || oneReachableAfterChangeDistances()
        unreachableAfterChange()  | 0         || unreachableAfterChangeDistances()
        manyUnreachable()         | 0         || manyUnreachableDistances()
        moreComplexGraph()        | 0         || moreComplexGraphDistances()
    }

    static def anyWithAllEdgesInBlue() {
        return [
                0: [new EdgeToNode(1, true), new EdgeToNode(2, true)],
                1: [new EdgeToNode(0, true), new EdgeToNode(3, true)],
                2: [new EdgeToNode(0, true)],
                3: [new EdgeToNode(1, true)],
                4: []
        ]
    }

    static def anyWithAllEdgesInBlueDistances() {
        return [0: 0, 1: 1, 2: 1, 3: 2, 4: -1]
    }

    static def oneUnreachableNode() {
        return [
                0: [new EdgeToNode(1, false), new EdgeToNode(2, true)],
                1: [new EdgeToNode(0, false), new EdgeToNode(3, false)],
                2: [new EdgeToNode(0, true)],
                3: [new EdgeToNode(1, false)],
        ]
    }

    static def oneUnreachableNodeDistances() {
        return [0: 0, 1: 1, 2: 1, 3: -1]
    }


    static def oneReachableAfterChange() {
        return [
                0: [new EdgeToNode(1, false), new EdgeToNode(2, true)],
                1: [new EdgeToNode(0, false), new EdgeToNode(3, true)],
                2: [new EdgeToNode(0, true), new EdgeToNode(3, true)],
                3: [new EdgeToNode(1, true), new EdgeToNode(2, true), new EdgeToNode(4, false)],
                4: [new EdgeToNode(3, false)]
        ]
    }

    static def oneReachableAfterChangeDistances() {
        return [0: 0, 1: 1, 2: 1, 3: 2, 4: 3]
    }


    static def unreachableAfterChange() {
        return [
                0: [new EdgeToNode(1, false), new EdgeToNode(2, true)],
                1: [new EdgeToNode(0, false), new EdgeToNode(3, true)],
                2: [new EdgeToNode(0, true), new EdgeToNode(3, true)],
                3: [new EdgeToNode(1, true), new EdgeToNode(2, true), new EdgeToNode(4, false)],
                4: [new EdgeToNode(3, false), new EdgeToNode(5, false)],
                5: [new EdgeToNode(4, false)]
        ]
    }

    static def unreachableAfterChangeDistances() {
        return [0: 0, 1: 1, 2: 1, 3: 2, 4: 3, 5: -1]
    }

    static def moreComplexGraph() {
        return [
                0: [new EdgeToNode(1, false), new EdgeToNode(2, false)],
                1: [new EdgeToNode(0, false), new EdgeToNode(3, false)],
                2: [new EdgeToNode(0, false), new EdgeToNode(4, true)],
                3: [new EdgeToNode(1, false), new EdgeToNode(5, true), new EdgeToNode(8, true), new EdgeToNode(4, true)],
                4: [new EdgeToNode(2, true), new EdgeToNode(3, true), new EdgeToNode(6, true)],
                5: [new EdgeToNode(3, true), new EdgeToNode(7, true)],
                6: [new EdgeToNode(8, true), new EdgeToNode(4, true)],
                7: [new EdgeToNode(5, true), new EdgeToNode(9, true)],
                8: [new EdgeToNode(3, true), new EdgeToNode(6, true), new EdgeToNode(9, false)],
                9: [new EdgeToNode(7, true), new EdgeToNode(8, false)],
        ]
    }

    static def moreComplexGraphDistances() {
        return [0: 0, 1: 1, 2: 1, 3: 3, 4: 2, 5: 4, 6: 3, 7: 5, 8: 4, 9: 6]
    }

    static def manyUnreachable() {
        return [
                0: [new EdgeToNode(1, false), new EdgeToNode(2, false)],
                1: [new EdgeToNode(0, false), new EdgeToNode(3, false)],
                2: [new EdgeToNode(0, false), new EdgeToNode(4, false)],
                3: [new EdgeToNode(1, false), new EdgeToNode(5, true)],
                4: [new EdgeToNode(2, false), new EdgeToNode(5, true)],
                5: [new EdgeToNode(3, true), new EdgeToNode(4, true)],

        ]
    }

    static def manyUnreachableDistances() {
        return [0: 0, 1: 1, 2: 1, 3: -1, 4: -1, 5: -1]
    }
}

