# Application purpose
This is a Java web application that implements a REST API. The application accepts inputs in JSON format, including:
1. An undirected and unweighted graph represented as an adjacency list. Each edge in the
   graph is colored either red or blue.
2. A start node ‘s‘ from where the distances are to be calculated.  

The expected output of the application, also in JSON format, is a list of all nodes in the graph.   
For each node, the output includes the shortest distance from the start node 's', considering paths with at most one red edge.

## Algorithm details
The algorithm is a modified version of a standard algorithm for finding the shortest paths in undirected graph (based on BFS).

Time complexity: O(rank + size). Explanation: the main route is the same as for BFS, which is of complexity O(rank+size), and number of calculations at each step is of complexity O(1).  
Space complexity: O(rank). Explanation: there are two extra maps and one list (queue), each of size equal to the rank of the graph.

Remarks:
1. Map implementation was chosen to provide flexibility in defining node labels. They are integers now, but it can be easily changed to strings, say.  
   On the other hand, if space becomes an issue, the implementation can be switched to arrays (as in the most common implementation).
2. The algorithm itself does not demand the graph to be undirected. In fact, it works without any modification also for directed graphs and multigraphs (= loops and multiple edges allowed).  
   That's why there is no need to check whether the provided graph is undirected.
3. Basic validations (= all values are mandatory) are performed on controller level. There are also two obvious constraints (data integrity) checked on service level:
   - start node should be a node in a graph
   - nodes from the lists (ends of edges) should be nodes of the graph

## Endpoint details
POST /calculator/calculateDistances JSON payload

Exemplary call:

curl --location "http://localhost:8080/calculator/calculateDistances" --header "Content-Type: application/json"  
--data "{\"startNode\":0,\"adjacencyLists\":{\"0\":[{\"node\":1,\"isBlue\":true},{\"node\":2,\"isBlue\":true}],\"1\":[{\"node\":0,\"isBlue\":true},{\"node\":3,\"isBlue\":true}],\"2\":[{\"node\":0,\"isBlue\":true}],\"3\":[{\"node\":1,\"isBlue\":true}],\"4\":[]}}"

### JSON schemas
*node* and *startNode* properties have integer values (any value possible), *isBlue* is boolean (*true/false*), *adjacencyList* is a map (key is a string containing integer, value is array of objects with two properties: *node* and *isBlue*)
Null values are not allowed, but arrays can be empty.

Payload schema:

```json
{
"startNode": integer1,
"adjacencyLists": {
   "integer1": [{"node": integer2, "isBlue": true}, {"node": integer3, "isBlue": false}],
   "integer2": [{"node": integer1, "isBlue": true}],
   "integer3": [{"node": integer1, "isBlue": false}],
   "integer4": []
   ...
   }
}
```

Response is a list *distances* of objects with two properties: *node* and *distance*. If there is a path between *startNode* and *node* then value of *distance* is a positive number (and 0 for *startNode* itself) otherwise the value is -1.

Response schema (for the payload given above)

```json
{
   "distances": [
      {
         "node": integer1,
         "distance": 0
      },
      {
         "node": integer2,
         "distance": 1
      },
      {
         "node": integer3,
         "distance": 1
      },
      {
         "node": integer4,
         "distance": -1
      }
      ...
   ]
}
```



